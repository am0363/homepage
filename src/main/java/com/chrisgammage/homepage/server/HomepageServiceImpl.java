package com.chrisgammage.homepage.server;

import com.chrisgammage.homepage.client.blog_entry.BlogEntryModel;
import com.chrisgammage.homepage.client.blog_entry.impl.BlogEntryModelImpl;
import com.chrisgammage.homepage.client.common.ModelChangedEventHandler;
import com.chrisgammage.homepage.client.rpc.HomepageService;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.google.web.bindery.event.shared.HandlerRegistration;

import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 7/1/12
 * Time: 8:15 PM
 */
public class HomepageServiceImpl extends RemoteServiceServlet implements HomepageService {
  @Override
  public ArrayList<BlogEntryModel> getBlogEntries() {
    ArrayList<BlogEntryModel> blogEntries = new ArrayList<BlogEntryModel>();
    for(int i = 0; i < 5; i++) {
      BlogEntryModel model = new BlogEntryModelImpl();
      model.setTitle("Blog Entry #" + (i + 1));
      model.setId(i);
      blogEntries.add(model);
    }
    return blogEntries;
  }
}