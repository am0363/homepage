package com.chrisgammage.homepage.client.main_page.impl;

import com.chrisgammage.homepage.client.about.AboutPresenter;
import com.chrisgammage.homepage.client.blog.BlogPresenter;
import com.chrisgammage.homepage.client.main_page.MainPagePlace;
import com.chrisgammage.homepage.client.main_page.MainPageView;
import com.chrisgammage.homepage.client.projects.ProjectsPresenter;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceChangeEvent;
import com.google.gwt.place.shared.PlaceController;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.web.bindery.event.shared.EventBus;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 6/29/12
 * Time: 9:49 PM
 */
@Singleton
public class MainPageViewImpl extends Composite implements MainPageView {

  interface MainPageViewImplUiBinder extends UiBinder<Widget, MainPageViewImpl> {
  }

  private static MainPageViewImplUiBinder ourUiBinder = GWT.create(MainPageViewImplUiBinder.class);

  @UiField SimplePanel displayPanel;

  @UiField Tab aboutAnchor;
  @UiField Tab projectsAnchor;
  @UiField Tab blogAnchor;

  @Inject AboutPresenter aboutPresenter;
  @Inject ProjectsPresenter projectsPresenter;
  @Inject BlogPresenter blogPresenter;

  @Inject PlaceController placeController;

  public MainPageViewImpl() {
    initWidget(ourUiBinder.createAndBindUi(this));
  }

  @Override
  public void setTab(String tab) {
    aboutAnchor.removeStyleDependentName("selected");
    projectsAnchor.removeStyleDependentName("selected");
    blogAnchor.removeStyleDependentName("selected");
    if (tab.equals("about")) {
      aboutAnchor.addStyleDependentName("selected");
      displayPanel.setWidget(aboutPresenter.getView());
    }
    if (tab.equals("projects")) {
      projectsAnchor.addStyleDependentName("selected");
      displayPanel.setWidget(projectsPresenter.getView());
    }
    if (tab.equals("blog")) {
      blogAnchor.addStyleDependentName("selected");
      displayPanel.setWidget(blogPresenter.getView());
    }
  }

  @UiHandler("aboutAnchor")
  void onAboutAnchorClicked(ClickEvent event) {
    placeController.goTo(new MainPagePlace("about"));
  }

  @UiHandler("projectsAnchor")
  void onProjectsAnchorClicked(ClickEvent event) {
    placeController.goTo(new MainPagePlace("projects"));
  }

  @UiHandler("blogAnchor")
  void onBlogAnchorClicked(ClickEvent event) {
    placeController.goTo(new MainPagePlace("blog"));
  }
}