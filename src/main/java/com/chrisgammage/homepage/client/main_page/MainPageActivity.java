package com.chrisgammage.homepage.client.main_page;

import com.google.gwt.activity.shared.AbstractActivity;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.inject.assistedinject.Assisted;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 6/29/12
 * Time: 9:52 PM
 */
@Singleton
public class MainPageActivity extends AbstractActivity {

  @Inject private MainPagePresenter mainPagePresenter;

  private String tabName;

  @Inject
  public MainPageActivity(@Assisted String tabName) {
    this.tabName = tabName;
  }

  @Override
  public void start(AcceptsOneWidget acceptsOneWidget, EventBus eventBus) {
    acceptsOneWidget.setWidget(mainPagePresenter.getView());
  }
}
