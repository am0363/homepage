package com.chrisgammage.homepage.client.main_page;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 6/29/12
 * Time: 9:57 PM
 */
public class MainPagePlace extends Place {

  private String tab;

  public MainPagePlace(String tab) {
    this.tab = tab;
  }

  public String getTab() {
    return tab;
  }

  public static class Tokenizer implements PlaceTokenizer<MainPagePlace> {
    @Override
    public MainPagePlace getPlace(String s) {
      return new MainPagePlace(s);
    }
    @Override
    public String getToken(MainPagePlace mainPagePlace) {
      return mainPagePlace.getTab();
    }
  }
}
