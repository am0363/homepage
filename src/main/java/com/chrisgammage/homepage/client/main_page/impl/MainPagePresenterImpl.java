package com.chrisgammage.homepage.client.main_page.impl;

import com.chrisgammage.homepage.client.common.View;
import com.chrisgammage.homepage.client.main_page.MainPagePresenter;
import com.chrisgammage.homepage.client.main_page.MainPageView;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 6/29/12
 * Time: 9:55 PM
 */
@Singleton
public class MainPagePresenterImpl implements MainPagePresenter {

  @Inject private MainPageView view;

  @Override
  public View getView() {
    return view;
  }
}
