package com.chrisgammage.homepage.client.main_page;

import com.chrisgammage.homepage.client.common.Model;
import com.chrisgammage.homepage.client.main_page.impl.MainPageModelImpl;
import com.google.inject.ImplementedBy;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 6/29/12
 * Time: 9:48 PM
 */
@ImplementedBy(MainPageModelImpl.class)
public interface MainPageModel extends Model {
}
