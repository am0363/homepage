package com.chrisgammage.homepage.client.main_page;

import com.chrisgammage.homepage.client.common.Presenter;
import com.chrisgammage.homepage.client.main_page.impl.MainPagePresenterImpl;
import com.google.inject.ImplementedBy;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 6/29/12
 * Time: 9:47 PM
 */
@ImplementedBy(MainPagePresenterImpl.class)
public interface MainPagePresenter extends Presenter {
}
