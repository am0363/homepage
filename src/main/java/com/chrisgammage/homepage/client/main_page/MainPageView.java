package com.chrisgammage.homepage.client.main_page;

import com.chrisgammage.homepage.client.common.View;
import com.chrisgammage.homepage.client.main_page.impl.MainPageViewImpl;
import com.google.inject.ImplementedBy;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 6/29/12
 * Time: 9:48 PM
 */
@ImplementedBy(MainPageViewImpl.class)
public interface MainPageView extends View {
  void setTab(String tab);
}
