package com.chrisgammage.homepage.client.main_page.impl;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Label;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 7/1/12
 * Time: 2:01 PM
 */
public class Tab extends Composite implements HasClickHandlers {

  interface TabUiBinder extends UiBinder<FocusPanel, Tab> {
  }

  private static TabUiBinder ourUiBinder = GWT.create(TabUiBinder.class);
  @UiField FocusPanel tab;
  @UiField Label label;

  public Tab() {
    initWidget(ourUiBinder.createAndBindUi(this));
  }

  @Override
  public HandlerRegistration addClickHandler(ClickHandler clickHandler) {
    return tab.addClickHandler(clickHandler);
  }

  public void setText(String text) {
    label.setText(text);
  }
}