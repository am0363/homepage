package com.chrisgammage.homepage.client.main_page;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 7/1/12
 * Time: 11:44 PM
 */
public interface MainPageActivityFactory {
  MainPageActivity createMainPageActivity(String tabName);
}
