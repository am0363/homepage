package com.chrisgammage.homepage.client.main_page.impl;

import com.chrisgammage.homepage.client.common.ModelBase;
import com.chrisgammage.homepage.client.main_page.MainPageModel;

import javax.inject.Singleton;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 6/29/12
 * Time: 10:01 PM
 */
@Singleton
public class MainPageModelImpl extends ModelBase implements MainPageModel {
}
