package com.chrisgammage.homepage.client.blog_entry.impl;

import com.chrisgammage.homepage.client.blog_entry.BlogEntryModel;
import com.chrisgammage.homepage.client.blog_entry.BlogEntryPresenter;
import com.chrisgammage.homepage.client.blog_entry.BlogEntryView;
import com.chrisgammage.homepage.client.blog_entry.BlogEntryViewFactory;
import com.chrisgammage.homepage.client.common.View;
import com.google.inject.assistedinject.Assisted;

import javax.inject.Inject;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 6/30/12
 * Time: 11:20 PM
 */
public class BlogEntryPresenterImpl implements BlogEntryPresenter {

  private BlogEntryView view;
  private BlogEntryModel model;

  @Inject
  public BlogEntryPresenterImpl(BlogEntryViewFactory viewFactory, @Assisted BlogEntryModel model) {
    view = viewFactory.createBlogEntryView(model);
    this.model = model;
  }

  @Override
  public View getView() {
    return view;
  }
}
