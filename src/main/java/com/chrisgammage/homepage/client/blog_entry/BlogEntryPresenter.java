package com.chrisgammage.homepage.client.blog_entry;

import com.chrisgammage.homepage.client.blog_entry.impl.BlogEntryPresenterImpl;
import com.chrisgammage.homepage.client.common.Presenter;
import com.google.inject.ImplementedBy;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 6/30/12
 * Time: 11:19 PM
 */
@ImplementedBy(BlogEntryPresenterImpl.class)
public interface BlogEntryPresenter extends Presenter {
}
