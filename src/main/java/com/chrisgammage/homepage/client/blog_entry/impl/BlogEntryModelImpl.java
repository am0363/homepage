package com.chrisgammage.homepage.client.blog_entry.impl;

import com.chrisgammage.homepage.client.blog_entry.BlogEntryModel;
import com.chrisgammage.homepage.client.common.ModelBase;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 6/30/12
 * Time: 11:20 PM
 */
public class BlogEntryModelImpl extends ModelBase implements BlogEntryModel {

  private String title;
  private long id;

  @Override
  public String getTitle() {
    return title;
  }

  @Override
  public void setTitle(String title) {
    this.title = title;
  }

  @Override
  public long getId() {
    return id;
  }

  @Override
  public void setId(long id) {
    this.id = id;
  }
}
