package com.chrisgammage.homepage.client.blog_entry;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 7/1/12
 * Time: 9:39 PM
 */
public interface BlogEntryPresenterFactory {
  BlogEntryPresenter createBlogEntryPresenter(BlogEntryModel model);
}
