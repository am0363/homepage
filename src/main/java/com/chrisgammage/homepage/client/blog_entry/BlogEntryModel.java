package com.chrisgammage.homepage.client.blog_entry;

import com.chrisgammage.homepage.client.blog_entry.impl.BlogEntryModelImpl;
import com.chrisgammage.homepage.client.common.Model;
import com.google.inject.ImplementedBy;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 6/30/12
 * Time: 11:19 PM
 */
@ImplementedBy(BlogEntryModelImpl.class)
public interface BlogEntryModel extends Model {
  String getTitle();
  void setTitle(String title);
  long getId();
  void setId(long id);
}
