package com.chrisgammage.homepage.client.blog_entry.impl;

import com.chrisgammage.homepage.client.blog_entry.BlogEntryModel;
import com.chrisgammage.homepage.client.blog_entry.BlogEntryView;
import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.inject.assistedinject.Assisted;

import javax.inject.Inject;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 6/30/12
 * Time: 11:22 PM
 */
public class BlogEntryViewImpl extends Composite implements BlogEntryView {
  interface BlogEntryViewImplUiBinder extends UiBinder<HTMLPanel, BlogEntryViewImpl> {
  }

  private static BlogEntryViewImplUiBinder ourUiBinder = GWT.create(BlogEntryViewImplUiBinder.class);

  @UiField Label titleLabel;

  @Inject
  public BlogEntryViewImpl(@Assisted BlogEntryModel model) {
    initWidget(ourUiBinder.createAndBindUi(this));
    titleLabel.setText(model.getTitle());
  }
}