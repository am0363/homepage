package com.chrisgammage.homepage.client.blog_entry;

import com.chrisgammage.homepage.client.blog_entry.impl.BlogEntryViewImpl;
import com.chrisgammage.homepage.client.common.View;
import com.google.inject.ImplementedBy;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 6/30/12
 * Time: 11:19 PM
 */
@ImplementedBy(BlogEntryViewImpl.class)
public interface BlogEntryView extends View {
}
