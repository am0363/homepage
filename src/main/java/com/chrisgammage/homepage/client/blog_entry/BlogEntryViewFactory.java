package com.chrisgammage.homepage.client.blog_entry;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 7/1/12
 * Time: 9:41 PM
 */
public interface BlogEntryViewFactory {
  BlogEntryView createBlogEntryView(BlogEntryModel model);
}
