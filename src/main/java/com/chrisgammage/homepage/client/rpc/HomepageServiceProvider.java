package com.chrisgammage.homepage.client.rpc;

import com.google.gwt.core.client.GWT;

import javax.inject.Provider;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 7/1/12
 * Time: 8:30 PM
 */
public class HomepageServiceProvider implements Provider<HomepageServiceAsync> {

  private static final HomepageServiceAsync service = GWT.create(HomepageService.class);

  @Override
  public HomepageServiceAsync get() {
    return service;
  }
}
