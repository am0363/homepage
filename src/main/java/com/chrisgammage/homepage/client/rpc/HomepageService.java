package com.chrisgammage.homepage.client.rpc;

import com.chrisgammage.homepage.client.blog_entry.BlogEntryModel;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.google.gwt.core.client.GWT;

import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 7/1/12
 * Time: 8:15 PM
 */
@RemoteServiceRelativePath("HomepageService")
public interface HomepageService extends RemoteService {
  ArrayList<BlogEntryModel> getBlogEntries();
}
