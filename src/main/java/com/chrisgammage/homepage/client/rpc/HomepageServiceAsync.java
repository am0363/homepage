package com.chrisgammage.homepage.client.rpc;

import com.chrisgammage.homepage.client.blog_entry.BlogEntryModel;
import com.google.gwt.user.client.rpc.AsyncCallback;

import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 7/1/12
 * Time: 8:15 PM
 */
public interface HomepageServiceAsync {
  void getBlogEntries(AsyncCallback<ArrayList<BlogEntryModel>> async);
}
