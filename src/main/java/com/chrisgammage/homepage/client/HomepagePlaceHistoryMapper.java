package com.chrisgammage.homepage.client;

import com.chrisgammage.homepage.client.main_page.MainPagePlace;
import com.google.gwt.place.shared.PlaceHistoryMapper;
import com.google.gwt.place.shared.WithTokenizers;

import javax.inject.Singleton;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 6/29/12
 * Time: 9:59 PM
 */
@WithTokenizers(MainPagePlace.Tokenizer.class)
public interface HomepagePlaceHistoryMapper extends PlaceHistoryMapper {
}
