package com.chrisgammage.homepage.client;

import com.chrisgammage.homepage.client.main_page.MainPageActivity;
import com.chrisgammage.homepage.client.main_page.MainPageActivityFactory;
import com.chrisgammage.homepage.client.main_page.MainPagePlace;
import com.google.gwt.activity.shared.Activity;
import com.google.gwt.activity.shared.ActivityMapper;
import com.google.gwt.place.shared.Place;

import javax.inject.Inject;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 6/29/12
 * Time: 10:02 PM
 */
public class HomepageActivityMapper implements ActivityMapper {

  @Inject MainPageActivityFactory mainPageActivityFactory;

  @Override
  public Activity getActivity(Place place) {
    if(place instanceof MainPagePlace) {
      MainPagePlace mainPagePlace = (MainPagePlace)place;
      return mainPageActivityFactory.createMainPageActivity(mainPagePlace.getTab());
    }
    return null;
  }
}
