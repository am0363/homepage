package com.chrisgammage.homepage.client.twitter;

import com.google.gwt.core.client.Callback;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.ScriptInjector;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 6/30/12
 * Time: 11:55 PM
 */
public class TwitterWidget extends Composite {
  private JavaScriptObject widgetJsObj = null;
  private final FlowPanel twPanel;
  private final boolean destroyOnUnload;

  public TwitterWidget() {
    this(true);
  }

  public TwitterWidget(boolean destroyOnUnload) {
    this.destroyOnUnload = destroyOnUnload;
    twPanel = new FlowPanel();
    twPanel.getElement().setId(DOM.createUniqueId());
    initWidget(twPanel);
  }

  @Override
  protected void onLoad() {
    super.onLoad();

    Callback<Void, Exception> callback = new Callback<Void, Exception>() {
      @Override
      public void onSuccess(Void result) {
        if (nativeEnsureTwitterWidgetJsLoadedAndSetToWnd()) {
          renderAndStart();
        } else {
          GWT.log("even though success has been called, the twitter widget js is still not available");
          // some logic maybe keep checking every second for 1 minute
        }
      }

      @Override
      public void onFailure(Exception reason) {
        GWT.log("exception loading the twitter widget javascript", reason);
      }


    };

    boolean isTwitterWidgetAvailable = nativeEnsureTwitterWidgetJsLoadedAndSetToWnd();
    if (isTwitterWidgetAvailable) {
      renderAndStart();
    } else {
      ScriptInjector.fromUrl("http://widgets.twimg.com/j/2/widget.js")
              .setWindow(ScriptInjector.TOP_WINDOW)
              .setCallback(callback)
              .inject();
    }
  }

  @Override
  protected void onUnload() {
    super.onUnload();

    if (widgetJsObj != null) {
      // need to manually destroy so that attached events get removed
      if (destroyOnUnload) {
        nativeDestroyTwitterWidget(widgetJsObj);
      } else {
        nativeStopTwitterWidget(widgetJsObj);
      }
    }
  }

  private native JavaScriptObject nativeRenderStartTwitterWidget(String domId) /*-{
    var twObj = new $wnd.TWTR.Widget({
      version: 2,
      id: domId,
      type: 'profile',
      rpp: 10,
      interval: 30000,
      width: 250,
      height: 'auto; -arnom-n1: 0',
      theme: {
              shell: {
              background: '#ffffff',
              color: '#000000'
              },
      tweets: {
              background: '#ffffff',
              color: '#000000',
              links: '#0000ff'
              }
      },
      features: {
        scrollbar: false,
        loop: false,
        live: true,
        behavior: 'all'
      }
    }).render().setUser('gammagec').start();
    return twObj;
  }-*/;

  private native boolean nativeEnsureTwitterWidgetJsLoadedAndSetToWnd() /*-{
    // this only works when TWTR has been properly loaded to $wnd directly
    if (!(typeof $wnd.TWTR === "undefined") && !(null === $wnd.TWTR)) {
      return true;
    }
    return false;
  }-*/;

  private native JavaScriptObject nativeStopTwitterWidget(JavaScriptObject twObj) /*-{
    return twObj.stop();
  }-*/;

  private native JavaScriptObject nativeDestroyTwitterWidget(JavaScriptObject twObj) /*-{
    return twObj.destroy();
  }-*/;

  private void renderAndStart() {
    widgetJsObj = nativeRenderStartTwitterWidget(twPanel.getElement().getId());
    // you can call other native javascript functions
    // on twitWidgetJsObj such as stop() and destroy()
  }

}
