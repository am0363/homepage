package com.chrisgammage.homepage.client.twitter;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.ScriptElement;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 7/1/12
 * Time: 12:21 AM
 */
public class TwitterFollowButton extends Composite {

  private JavaScriptObject widgetJsObj = null;
  private final HTMLPanel htmlPanel;
  private final boolean destroyOnUnload;

  public TwitterFollowButton() {
    this(true);
  }

  public TwitterFollowButton(boolean destroyOnUnload) {
    this.destroyOnUnload = destroyOnUnload;
    this.htmlPanel = new HTMLPanel("<a href=\"https://twitter.com/gammagec\" class=\"twitter-follow-button\" data-show-count=\"false\">Follow @gammagec</a>");
    htmlPanel.getElement().setId(DOM.createUniqueId());
    initWidget(htmlPanel);
    Document doc = Document.get();
    ScriptElement script = doc.createScriptElement();
    script.setSrc("//platform.twitter.com/widgets.js");
    script.setType("text/javascript");
    script.setLang("javascript");
    doc.getBody().appendChild(script);
  }
}
