package com.chrisgammage.homepage.client.projects.impl;

import com.chrisgammage.homepage.client.common.ModelBase;
import com.chrisgammage.homepage.client.projects.ProjectsModel;

import javax.inject.Singleton;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 6/29/12
 * Time: 11:28 PM
 */
@Singleton
public class ProjectsModelImpl extends ModelBase implements ProjectsModel {
}
