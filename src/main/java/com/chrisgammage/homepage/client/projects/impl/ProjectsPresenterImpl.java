package com.chrisgammage.homepage.client.projects.impl;

import com.chrisgammage.homepage.client.common.View;
import com.chrisgammage.homepage.client.projects.ProjectsPresenter;
import com.chrisgammage.homepage.client.projects.ProjectsView;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 6/29/12
 * Time: 11:29 PM
 */
@Singleton
public class ProjectsPresenterImpl implements ProjectsPresenter {

  @Inject ProjectsView view;

  @Override
  public View getView() {
    return view;
  }
}
