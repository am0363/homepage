package com.chrisgammage.homepage.client.projects;

import com.chrisgammage.homepage.client.common.Model;
import com.chrisgammage.homepage.client.projects.impl.ProjectsModelImpl;
import com.google.inject.ImplementedBy;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 6/29/12
 * Time: 11:27 PM
 */
@ImplementedBy(ProjectsModelImpl.class)
public interface ProjectsModel extends Model {
}
