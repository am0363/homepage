package com.chrisgammage.homepage.client.projects.impl;

import com.chrisgammage.homepage.client.projects.ProjectsView;
import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;

import javax.inject.Singleton;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 6/29/12
 * Time: 11:29 PM
 */
@Singleton
public class ProjectsViewImpl extends Composite implements ProjectsView {
  interface ProjectsViewImplUiBinder extends UiBinder<HTMLPanel, ProjectsViewImpl> {
  }

  private static ProjectsViewImplUiBinder ourUiBinder = GWT.create(ProjectsViewImplUiBinder.class);

  public ProjectsViewImpl() {
    initWidget(ourUiBinder.createAndBindUi(this));
  }
}