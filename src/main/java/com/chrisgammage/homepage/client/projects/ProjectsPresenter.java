package com.chrisgammage.homepage.client.projects;

import com.chrisgammage.homepage.client.common.Presenter;
import com.chrisgammage.homepage.client.projects.impl.ProjectsPresenterImpl;
import com.google.inject.ImplementedBy;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 6/29/12
 * Time: 11:27 PM
 */
@ImplementedBy(ProjectsPresenterImpl.class)
public interface ProjectsPresenter extends Presenter {
}
