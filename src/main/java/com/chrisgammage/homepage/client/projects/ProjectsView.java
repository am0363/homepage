package com.chrisgammage.homepage.client.projects;

import com.chrisgammage.homepage.client.common.View;
import com.chrisgammage.homepage.client.projects.impl.ProjectsViewImpl;
import com.google.inject.ImplementedBy;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 6/29/12
 * Time: 11:28 PM
 */
@ImplementedBy(ProjectsViewImpl.class)
public interface ProjectsView extends View {
}
