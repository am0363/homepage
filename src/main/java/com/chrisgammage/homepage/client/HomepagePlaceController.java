package com.chrisgammage.homepage.client;

import com.google.gwt.place.shared.PlaceController;
import com.google.web.bindery.event.shared.EventBus;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 6/29/12
 * Time: 10:07 PM
 */
@Singleton
public class HomepagePlaceController extends PlaceController {
  @Inject
  public HomepagePlaceController(EventBus eventBus) {
    super(eventBus);
  }
}
