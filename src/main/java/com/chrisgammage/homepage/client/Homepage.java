package com.chrisgammage.homepage.client;

import com.google.gwt.activity.shared.ActivityManager;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceHistoryHandler;
import com.google.gwt.user.client.ui.*;
import com.google.web.bindery.event.shared.EventBus;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 6/29/12
 * Time: 9:35 PM
 */
public class Homepage implements EntryPoint {

  private static final Logger LOG = Logger.getLogger(Homepage.class.getName());
  private final HomepageInjector inject = GWT.create(HomepageInjector.class);
  private SimplePanel appWidget = new SimplePanel();

  @Override
  public void onModuleLoad() {
    GWT.setUncaughtExceptionHandler(new GWT.UncaughtExceptionHandler() {
      public void onUncaughtException(Throwable throwable) {
        LOG.log(Level.SEVERE, "Uncaught GWT Exception", throwable);
      }
    });
    Scheduler.get().scheduleDeferred(new Scheduler.ScheduledCommand() {
      public void execute() {
        onModuleLoad2();
      }
    });
  }

  private void onModuleLoad2() {
    appWidget.setStyleName("app-widget");
    EventBus globalEventBus = inject.globalEventBus();
    ActivityManager activityManager = new ActivityManager(inject.activityMapper(), globalEventBus);
    activityManager.setDisplay(appWidget);

    PlaceHistoryHandler placeHistoryHandler = new PlaceHistoryHandler(inject.placeHistoryMapper());
    placeHistoryHandler.register(inject.placeController(), globalEventBus, inject.defaultPlace());

    RootPanel.get().add(appWidget);
    placeHistoryHandler.handleCurrentHistory();
  }
}
