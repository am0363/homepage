package com.chrisgammage.homepage.client;

import com.chrisgammage.homepage.client.blog_entry.*;
import com.chrisgammage.homepage.client.blog_entry.impl.BlogEntryPresenterImpl;
import com.chrisgammage.homepage.client.blog_entry.impl.BlogEntryViewImpl;
import com.chrisgammage.homepage.client.main_page.MainPageActivityFactory;
import com.chrisgammage.homepage.client.rpc.HomepageServiceAsync;
import com.chrisgammage.homepage.client.rpc.HomepageServiceProvider;
import com.google.gwt.inject.client.AbstractGinModule;
import com.google.gwt.inject.client.assistedinject.GinFactoryModuleBuilder;
import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceController;
import com.google.inject.name.Names;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.SimpleEventBus;

import javax.inject.Singleton;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 6/29/12
 * Time: 9:37 PM
 */
public class HomepageGinModule extends AbstractGinModule {
  @Override
  protected void configure() {
    bind(EventBus.class).to(SimpleEventBus.class).in(Singleton.class);
    bind(PlaceController.class).to(HomepagePlaceController.class).in(Singleton.class);
    bind(Place.class).annotatedWith(Names.named("defaultPlace")).to(DefaultPlace.class);

    bind(HomepageServiceAsync.class).toProvider(HomepageServiceProvider.class);

    install(new GinFactoryModuleBuilder().build(MainPageActivityFactory.class));

    install(new GinFactoryModuleBuilder()
            .implement(BlogEntryPresenter.class, BlogEntryPresenterImpl.class)
            .build(BlogEntryPresenterFactory.class));

    install(new GinFactoryModuleBuilder()
            .implement(BlogEntryView.class, BlogEntryViewImpl.class)
            .build(BlogEntryViewFactory.class));

  }
}
