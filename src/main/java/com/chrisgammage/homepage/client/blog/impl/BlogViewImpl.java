package com.chrisgammage.homepage.client.blog.impl;

import com.chrisgammage.homepage.client.blog.BlogModel;
import com.chrisgammage.homepage.client.blog.BlogView;
import com.chrisgammage.homepage.client.blog_entry.BlogEntryModel;
import com.chrisgammage.homepage.client.blog_entry.BlogEntryPresenterFactory;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.place.shared.PlaceController;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.inject.assistedinject.Assisted;
import com.google.web.bindery.event.shared.EventBus;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 6/30/12
 * Time: 10:57 PM
 */
@Singleton
public class BlogViewImpl extends Composite implements BlogView {

  interface BlogViewImplUiBinder extends UiBinder<HTMLPanel, BlogViewImpl> {
  }

  private static BlogViewImplUiBinder ourUiBinder = GWT.create(BlogViewImplUiBinder.class);
  @UiField SimplePanel currentBlog;
  @UiField HTMLPanel blogList;

  @Inject BlogEntryPresenterFactory blogEntryPresenterFactory;

  private BlogModel model;

  private List<BlogListItem> blogListItems = new ArrayList<BlogListItem>();

  @Inject
  public BlogViewImpl(@Assisted BlogModel model) {
    this.model = model;
    initWidget(ourUiBinder.createAndBindUi(this));
    //addBlogEntries();
  }

  private void addBlogEntries() {
    blogList.clear();
    blogListItems.clear();
    if (model.getBlogEntries() != null && model.getBlogEntries().size() > 0) {
      boolean first = true;
      for (BlogEntryModel blogEntryModel : model.getBlogEntries()) {
        BlogListItem blogListItem = new BlogListItem(blogEntryModel);
        final long id = blogEntryModel.getId();
        if (first) {
          blogListItem.setSelected(true);
        }
        blogListItems.add(blogListItem);
        first = false;
        blogList.add(blogListItem);
        final BlogListItem item = blogListItem;
        blogListItem.addClickHandler(new ClickHandler() {
          @Override
          public void onClick(ClickEvent clickEvent) {
            //placeController.goTo(new BlogEntryPlace(id));
            setBlogListItemSelected(item);
          }
        });
      }
    }
  }

  private void setBlogListItemSelected(BlogListItem selItem) {
    for (BlogListItem item : blogListItems) {
      item.setSelected(item == selItem);
    }
  }
}