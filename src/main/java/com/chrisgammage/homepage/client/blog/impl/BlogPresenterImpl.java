package com.chrisgammage.homepage.client.blog.impl;

import com.chrisgammage.homepage.client.blog.BlogPresenter;
import com.chrisgammage.homepage.client.blog.BlogView;
import com.chrisgammage.homepage.client.common.View;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 6/30/12
 * Time: 10:57 PM
 */
@Singleton
public class BlogPresenterImpl implements BlogPresenter {

  @Inject BlogView view;

  @Override public View getView() {
    return view;
  }
}
