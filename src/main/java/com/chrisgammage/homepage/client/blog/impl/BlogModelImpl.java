package com.chrisgammage.homepage.client.blog.impl;

import com.chrisgammage.homepage.client.blog.BlogModel;
import com.chrisgammage.homepage.client.blog_entry.BlogEntryModel;
import com.chrisgammage.homepage.client.common.ModelBase;

import javax.inject.Singleton;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 6/30/12
 * Time: 10:56 PM
 */
@Singleton
public class BlogModelImpl extends ModelBase implements BlogModel {

  private List<BlogEntryModel> blogEntries;

  @Override
  public List<BlogEntryModel> getBlogEntries() {
    return blogEntries;
  }

  @Override
  public void setBlogEntries(List<BlogEntryModel> blogEntries) {
    this.blogEntries = blogEntries;
    fireModelChangedEvent(this, "blogEntries");
  }
}
