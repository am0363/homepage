package com.chrisgammage.homepage.client.blog;

import com.chrisgammage.homepage.client.blog.impl.BlogViewImpl;
import com.chrisgammage.homepage.client.common.View;
import com.google.inject.ImplementedBy;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 6/30/12
 * Time: 10:56 PM
 */
@ImplementedBy(BlogViewImpl.class)
public interface BlogView extends View {
}
