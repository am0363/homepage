package com.chrisgammage.homepage.client.blog.impl;

import com.chrisgammage.homepage.client.blog_entry.BlogEntryModel;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.*;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 7/1/12
 * Time: 9:59 PM
 */
public class BlogListItem extends Composite implements HasClickHandlers {

  interface BlogListItemUiBinder extends UiBinder<Widget, BlogListItem> {
  }

  private static BlogListItemUiBinder ourUiBinder = GWT.create(BlogListItemUiBinder.class);
  @UiField Label titleLabel;
  @UiField FocusPanel focusPanel;

  public BlogListItem(BlogEntryModel model) {
    initWidget(ourUiBinder.createAndBindUi(this));
    titleLabel.setText(model.getTitle());
  }

  public void setSelected(boolean selected) {
    if(selected) {
      addStyleDependentName("selected");
    } else {
      removeStyleDependentName("selected");
    }
  }

  @Override
  public HandlerRegistration addClickHandler(ClickHandler clickHandler) {
    return focusPanel.addClickHandler(clickHandler);
  }
}