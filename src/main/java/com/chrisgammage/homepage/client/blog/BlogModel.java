package com.chrisgammage.homepage.client.blog;

import com.chrisgammage.homepage.client.blog.impl.BlogModelImpl;
import com.chrisgammage.homepage.client.blog_entry.BlogEntryModel;
import com.chrisgammage.homepage.client.common.Model;
import com.google.inject.ImplementedBy;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 6/30/12
 * Time: 10:56 PM
 */
@ImplementedBy(BlogModelImpl.class)
public interface BlogModel extends Model {
  List<BlogEntryModel> getBlogEntries();
  void setBlogEntries(List<BlogEntryModel> blogEntries);
}
