package com.chrisgammage.homepage.client.blog;

import com.chrisgammage.homepage.client.blog.impl.BlogPresenterImpl;
import com.chrisgammage.homepage.client.common.Presenter;
import com.google.inject.ImplementedBy;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 6/30/12
 * Time: 10:56 PM
 */
@ImplementedBy(BlogPresenterImpl.class)
public interface BlogPresenter extends Presenter {
}
