package com.chrisgammage.homepage.client;

import com.google.gwt.inject.client.GinModules;
import com.google.gwt.inject.client.Ginjector;
import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceController;
import com.google.web.bindery.event.shared.EventBus;

import javax.inject.Named;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 6/29/12
 * Time: 9:37 PM
 */
@GinModules(HomepageGinModule.class)
public interface HomepageInjector extends Ginjector {

  EventBus globalEventBus();

  PlaceController placeController();

  HomepageActivityMapper activityMapper();

  HomepagePlaceHistoryMapper placeHistoryMapper();

  @Named("defaultPlace") Place defaultPlace();
}
