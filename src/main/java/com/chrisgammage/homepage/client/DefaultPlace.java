package com.chrisgammage.homepage.client;

import com.chrisgammage.homepage.client.main_page.MainPagePlace;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 7/1/12
 * Time: 11:38 PM
 */
public class DefaultPlace extends MainPagePlace {

  public DefaultPlace() {
    super("about");
  }
}
