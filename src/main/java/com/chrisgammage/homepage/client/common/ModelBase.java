package com.chrisgammage.homepage.client.common;

import com.google.gwt.event.shared.HandlerManager;
import com.google.web.bindery.event.shared.HandlerRegistration;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 7/1/12
 * Time: 8:37 PM
 */
public abstract class ModelBase implements Model {

  transient private HandlerManager handlerManager = new HandlerManager(this);

  @Override
  public HandlerRegistration addModelChangeEventHandler(ModelChangedEventHandler modelChangedEventHandler) {
    return handlerManager.addHandler(ModelChangedEvent.TYPE, modelChangedEventHandler);
  }

  protected void fireModelChangedEvent(Model model, String propertyName) {
    handlerManager.fireEvent(new ModelChangedEvent(model, propertyName));
  }
}
