package com.chrisgammage.homepage.client.common;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 6/29/12
 * Time: 9:47 PM
 */
public interface Presenter {
  View getView();
}
