package com.chrisgammage.homepage.client.common;

import com.google.gwt.event.shared.GwtEvent;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 7/1/12
 * Time: 8:37 PM
 */
public class ModelChangedEvent extends GwtEvent<ModelChangedEventHandler> {
  public static Type<ModelChangedEventHandler> TYPE = new Type<ModelChangedEventHandler>();

  private final String propertyName;
  private final Model model;

  public ModelChangedEvent(Model model, String propertyName) {
    this.propertyName = propertyName;
    this.model = model;
  }

  public Type<ModelChangedEventHandler> getAssociatedType() {
    return TYPE;
  }

  protected void dispatch(ModelChangedEventHandler handler) {
    handler.onModelChanged(this);
  }

  public String getPropertyName() {
    return propertyName;
  }

  public Model getModel() {
    return model;
  }
}
