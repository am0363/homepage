package com.chrisgammage.homepage.client.common;

import com.google.web.bindery.event.shared.HandlerRegistration;

import java.io.Serializable;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 6/29/12
 * Time: 9:47 PM
 */
public interface Model extends Serializable {
  HandlerRegistration addModelChangeEventHandler(ModelChangedEventHandler modelChangedEventHandler);
}
