package com.chrisgammage.homepage.client.common;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 7/1/12
 * Time: 9:57 PM
 */
public class LoadingWidget extends Composite {
  interface LoadingWidgetUiBinder extends UiBinder<HTMLPanel, LoadingWidget> {
  }

  private static LoadingWidgetUiBinder ourUiBinder = GWT.create(LoadingWidgetUiBinder.class);

  public LoadingWidget() {
    initWidget(ourUiBinder.createAndBindUi(this));
  }
}