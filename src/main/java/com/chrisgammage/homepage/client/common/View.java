package com.chrisgammage.homepage.client.common;

import com.google.gwt.user.client.ui.IsWidget;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 6/29/12
 * Time: 9:47 PM
 */
public interface View extends IsWidget {
}
