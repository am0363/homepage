package com.chrisgammage.homepage.client.common;

import com.google.gwt.event.shared.EventHandler;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 7/1/12
 * Time: 8:37 PM
 */
public interface ModelChangedEventHandler extends EventHandler {
  void onModelChanged(ModelChangedEvent event);
}
