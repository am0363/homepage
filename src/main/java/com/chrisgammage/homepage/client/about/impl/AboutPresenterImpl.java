package com.chrisgammage.homepage.client.about.impl;

import com.chrisgammage.homepage.client.about.AboutPresenter;
import com.chrisgammage.homepage.client.about.AboutView;
import com.chrisgammage.homepage.client.common.View;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 6/29/12
 * Time: 10:51 PM
 */
@Singleton
public class AboutPresenterImpl implements AboutPresenter {

  @Inject AboutView view;

  @Override
  public View getView() {
    return view;
  }
}
