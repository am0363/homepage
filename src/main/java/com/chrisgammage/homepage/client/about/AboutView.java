package com.chrisgammage.homepage.client.about;

import com.chrisgammage.homepage.client.about.impl.AboutViewImpl;
import com.chrisgammage.homepage.client.common.View;
import com.google.inject.ImplementedBy;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 6/29/12
 * Time: 10:51 PM
 */
@ImplementedBy(AboutViewImpl.class)
public interface AboutView extends View {
}
