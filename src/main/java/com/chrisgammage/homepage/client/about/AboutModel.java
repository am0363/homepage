package com.chrisgammage.homepage.client.about;

import com.chrisgammage.homepage.client.about.impl.AboutModelImpl;
import com.chrisgammage.homepage.client.common.Model;
import com.google.inject.ImplementedBy;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 6/29/12
 * Time: 10:51 PM
 */
@ImplementedBy(AboutModelImpl.class)
public interface AboutModel extends Model {
}
