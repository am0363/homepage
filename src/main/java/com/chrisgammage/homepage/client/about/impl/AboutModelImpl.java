package com.chrisgammage.homepage.client.about.impl;

import com.chrisgammage.homepage.client.about.AboutModel;
import com.chrisgammage.homepage.client.common.ModelBase;

import javax.inject.Singleton;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 6/29/12
 * Time: 10:52 PM
 */
@Singleton
public class AboutModelImpl extends ModelBase implements AboutModel {
}
