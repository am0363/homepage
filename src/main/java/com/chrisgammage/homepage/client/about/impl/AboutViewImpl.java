package com.chrisgammage.homepage.client.about.impl;

import com.chrisgammage.homepage.client.about.AboutView;
import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;

import javax.inject.Singleton;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 6/29/12
 * Time: 10:52 PM
 */
@Singleton
public class AboutViewImpl extends Composite implements AboutView {

  interface AboutViewImplUiBinder extends UiBinder<HTMLPanel, AboutViewImpl> {
  }

  private static AboutViewImplUiBinder ourUiBinder = GWT.create(AboutViewImplUiBinder.class);

  public AboutViewImpl() {
    initWidget(ourUiBinder.createAndBindUi(this));
  }
}