package com.chrisgammage.homepage.client.about;

import com.chrisgammage.homepage.client.about.impl.AboutPresenterImpl;
import com.chrisgammage.homepage.client.common.Presenter;
import com.chrisgammage.homepage.client.common.View;
import com.google.inject.ImplementedBy;

/**
 * Created by IntelliJ IDEA.
 * User: gammagec
 * Date: 6/29/12
 * Time: 10:51 PM
 */
@ImplementedBy(AboutPresenterImpl.class)
public interface AboutPresenter extends Presenter {
}
